package application;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalTime;

import controller.Controller;
import view.MainView;

public class Application_2 {
	//make view visible
	
	public static void writeTextToFile(String text) {
		try{
            BufferedWriter writer = new BufferedWriter(new FileWriter("simulation.txt", true));
            writer.append(text);
            writer.close();

        }catch (IOException e){
            System.out.println(e.getMessage());
        }
	}
	
	public static void main(String args[]) {
		MainView mainView = new MainView("Realistic Queue Simulator and Analyser");
		Controller controller = new Controller(mainView);
		mainView.setVisible(true);
		controller.initializeActionListeners();
	}
}
