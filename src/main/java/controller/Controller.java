package controller;

import model.Cashier;
import model.Client;
import model.ObservableQueue;
import model.QueueObserver;
import view.MainView;

import java.awt.EventQueue;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import application.Application_2;

public class Controller implements QueueObserver<Client> {
	
	private MainView mainView;
	volatile private int totalServiceTime;
	volatile private int totalWaitingTime;
	volatile private int totalNumberOfClients;
	volatile private int currentNumberOfClients;
	volatile private int peakHourMax;
	volatile private int totalEmptyTime;
	volatile private LocalTime peakHourTime;
	volatile private LocalTime beginningTime;
	
	
	public Controller(MainView mainView) {
				
		this.mainView = mainView;
	}
	private List<ObservableQueue<Client>> initObservableQueues(int numberOfQueues) {
		int i = numberOfQueues;
		List<ObservableQueue<Client>> aux = new ArrayList<ObservableQueue<Client>>();
		while(i>0) {
			ObservableQueue<Client> each = new ObservableQueue<Client>();
			each.addObserver(this);
			aux.add(each);
			i--;
		}
		return aux;
	}
	
	private List<Cashier> initCashiers(List<ObservableQueue<Client>> queues,int numberOfQueues){
		List<Cashier> cashiers = new ArrayList<Cashier>();
		int i = numberOfQueues-1;
		while (i >= 0) {
			Cashier cashier = new Cashier(queues.get(i));
			cashiers.add(cashier);
			i--;
		}
		return cashiers;
	}
	
	private void getDataAndSimulate() {
		final int minArrivalTimeBetweenCustomers = Integer.parseInt(this.mainView.getMinTimeText());
		final int maxArrivalTimeBetweenCustomers = Integer.parseInt(this.mainView.getMaxTimeText());
		final int minServiceTime = Integer.parseInt(this.mainView.getMinServiceText());
		final int maxServiceTime = Integer.parseInt(this.mainView.getMaxServiceText());
		final int numberOfQueues = this.mainView.getNumberOfQueues();
		final int simulationTime = Integer.parseInt(this.mainView.getSimulationTimeText());
		ObservableQueue.resetId();
		Client.resetClientId();
		mainView.setBtnBeginClickable(false);
		mainView.initializeQueueBoxes(numberOfQueues);
				
		final Thread simThread = new Thread(new Runnable() {
			public void run() {
			simulate(minArrivalTimeBetweenCustomers,maxArrivalTimeBetweenCustomers,minServiceTime,maxServiceTime,numberOfQueues,simulationTime);
			}
		});
		simThread.start();
		TimerTask task = new TimerTask() {
			public void run() {
				try {
					//System.out.println("Simulation is closing..\n");
					Application_2.writeTextToFile("Simulation is closing..\n"+"\n");
					simThread.join();
				} catch (InterruptedException e) {
					
					e.printStackTrace();
				}
				mainView.setBtnBeginClickable(true);
			}
		};
		Timer timer = new Timer();
		timer.schedule(task, (simulationTime) * (1000));
	}
	public void initializeActionListeners() {
		mainView.btnBeginActionListener(e -> {
			
		getDataAndSimulate();
		
	});}
	
	private void printSimulationResults() {
//		System.out.println("\n*********************************");
//		System.out.println("Simulation is over! Here are the results:");
//		System.out.println("Average waiting time = " + (float)this.totalWaitingTime / this.totalNumberOfClients);
//		System.out.println("Average service time = " + (float)this.totalServiceTime / this.totalNumberOfClients);
//		System.out.println("Empty queue time = " + this.totalEmptyTime);
//		System.out.println("Peak hour was at = " + (int)Duration.between(this.beginningTime, this.peakHourTime).getSeconds());
//		System.out.println("Max number of clients = " + this.peakHourMax);
//		System.out.println("*********************************");
		Application_2.writeTextToFile("\n*********************************"+"\n");
		Application_2.writeTextToFile("Simulation is over! Here are the results:"+"\n");
		Application_2.writeTextToFile("Average waiting time = " + (float)this.totalWaitingTime / this.totalNumberOfClients+"\n");
		Application_2.writeTextToFile("Average service time = " + (float)this.totalServiceTime / this.totalNumberOfClients+"\n");
		Application_2.writeTextToFile("Empty queue time = " + this.totalEmptyTime+"\n");
		Application_2.writeTextToFile("Peak hour was at = " + (int)Duration.between(this.beginningTime, this.peakHourTime).getSeconds()+"\n");
		Application_2.writeTextToFile("Max number of clients = " + this.peakHourMax+"\n");
		Application_2.writeTextToFile("*********************************"+"\n"+"\n");
		
	}
	
	private void simulate(int minArrivalTimeBetweenCustomers,int maxArrivalTimeBetweenCustomers,int minServiceTime,int maxServiceTime,int numberOfQueues,int simulationTime) {
		//get data from mainView
		// preparing simulation data
		this.totalServiceTime = 0;
		this.totalWaitingTime = 0;
		this.totalNumberOfClients = 0;
		this.currentNumberOfClients = 0;
		this.peakHourMax = 0;
		this.totalEmptyTime = 0;
		this.beginningTime = LocalTime.now();
		Application_2.writeTextToFile("*********************************\n");
		Application_2.writeTextToFile("Starting new simulation..\n");
		List<ObservableQueue<Client>> queues = initObservableQueues(numberOfQueues);
		List<Cashier> cashiers = initCashiers(queues,numberOfQueues);
		
		DoorBoy doorBoy = new DoorBoy(minArrivalTimeBetweenCustomers, maxArrivalTimeBetweenCustomers,minServiceTime,maxServiceTime, queues);
		// running simulation
		Thread producer = new Thread(doorBoy);
		producer.start();
		List<Thread> cashierThreads = new ArrayList<Thread>();
		for (Cashier cashier : cashiers) {
			Thread thread = new Thread(cashier);
			cashierThreads.add(thread);
			thread.start();
		}
		try {
			Thread.sleep(simulationTime*1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			doorBoy.stop();
			for(Cashier cashier : cashiers) {
				cashier.stop();
			}
			int i=0;
			for(Thread thread:cashierThreads) {
				try {
					thread.join();
					this.totalEmptyTime += cashiers.get(i).getEmptyTime();
					i+=1;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			try {
				producer.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			printSimulationResults();
		}
	}

	@Override
	public void pushedElement(ObservableQueue<Client> queue, Client e) {
		// TODO Auto-generated method stub
		this.currentNumberOfClients +=1;
		if(this.currentNumberOfClients > this.peakHourMax) {
			this.peakHourMax = this.currentNumberOfClients;
			this.peakHourTime = LocalTime.now();
		}
		
		//System.out.println("Client "+ e.getId() + " was added to "+ queue.getId());
		Application_2.writeTextToFile("Client "+ e.getId() + " was added to "+ queue.getId()+"\n");
		mainView.pushLabelOnBox(queue.getId(), e.toString());
	}

	@Override
	public void poppedElement(ObservableQueue<Client> queue, Client e) {
		// TODO Auto-generated method stub
		this.totalNumberOfClients+=1;
		this.currentNumberOfClients -=1;
		if(this.currentNumberOfClients > this.peakHourMax) {
			this.peakHourMax = this.currentNumberOfClients;
			this.peakHourTime = LocalTime.now();
		}
		this.totalServiceTime += e.getProcessingTime();
		this.totalWaitingTime +=(int)Duration.between(e.getTimeAtArrival(),LocalTime.now()).getSeconds();
		//System.out.println("Client "+ e.getId() + " has been processed from "+ queue.getId());
		Application_2.writeTextToFile("Client "+ e.getId() + " has been processed from "+ queue.getId() +"\n");
		mainView.popLabelOnBox(queue.getId());
	}
}
