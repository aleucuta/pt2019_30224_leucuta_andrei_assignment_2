package controller;

import java.time.Duration;
import java.time.LocalTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

import application.Application_2;
import model.Client;
import model.ObservableQueue;

public class DoorBoy implements Runnable {
	private int minArrivalTimeBetweenCustomers;
	private int maxArrivalTimeBetweenCustomers;
	private int minServiceTime;
	private int maxServiceTime;
	private List<ObservableQueue<Client>> queues;
	private final AtomicBoolean running = new AtomicBoolean(false);
	private LocalTime startTime;

	public void stop() {
		running.set(false);
	}

	private Client generateClient() throws InterruptedException {		
		Random rand = new Random();
		int delay = rand.nextInt(maxArrivalTimeBetweenCustomers - minArrivalTimeBetweenCustomers + 1);
		delay += minArrivalTimeBetweenCustomers;
		int processingTime = rand.nextInt(maxServiceTime - minServiceTime + 1);
		processingTime += minServiceTime;
		
		if (delay > 0)
			Thread.sleep(delay * 100);
		
		Client client = new Client((int) Duration.between(startTime,LocalTime.now()).getSeconds(), processingTime);
		//System.out.println(client.toString());
		Application_2.writeTextToFile(client.toString()+"\n");
		return client;
	}

	private ObservableQueue<Client> getMinQueue() {
		ObservableQueue<Client> minQueue = null;
		int minSize = 9999;
		for (ObservableQueue<Client> each : this.queues) {
			int auxSize = each.getSize();
			if (auxSize < minSize) {
				minQueue = each;
				minSize = auxSize;
			}
		}
		return minQueue;
	}

	public DoorBoy(int minArrivalTime, int maxArrivalTime, int minServiceTime, int maxServiceTime,List<ObservableQueue<Client>> queues) {
		this.minArrivalTimeBetweenCustomers = minArrivalTime;
		this.maxArrivalTimeBetweenCustomers = maxArrivalTime;
		this.minServiceTime = minServiceTime;
		this.maxServiceTime = maxServiceTime;
		this.queues = queues;
	}

	@Override
	public void run() {
		Client client = null;
		running.set(true);
		
		startTime = LocalTime.now();
		while (running.get()) {
			try {
				client = generateClient();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				running.set(false);
				continue;
			}

			if (running.get()) {
				ObservableQueue<Client> minQueue = getMinQueue();
				minQueue.pushElement(client);
			}
		}

	}

}
