package model;

import java.time.Duration;
import java.time.LocalTime;
import java.util.concurrent.atomic.AtomicBoolean;

import application.Application_2;

public class Cashier implements Runnable{
	
	private int id;
	private ObservableQueue<Client> clients;
	private final AtomicBoolean running = new AtomicBoolean(false);
	private LocalTime lastBusyTime;
	private int totalEmptyTime;
	
	
	//private Executor queueProcessor = Executors.newSingleThreadScheduledExecutor();
	
	public Cashier(ObservableQueue<Client> queue) {
		this.id = queue.getId();
		this.clients = queue;
		this.lastBusyTime = LocalTime.now();
		this.totalEmptyTime = 0;
	}
	
	public int getId() {
		return this.id;
	}
	
	public void stop() {
		this.running.set(false);
	}
	
	public int getEmptyTime() {
		return this.totalEmptyTime;
	}
	
	public void run() {
		// TODO Auto-generated method stub
		this.running.set(true);
		while(this.running.get()) {
			while(this.running.get() && clients.isEmpty()) {
				
			}
			if(this.running.get() == false){
				break;
			}
			totalEmptyTime+=(int)Duration.between(this.lastBusyTime,LocalTime.now()).getSeconds();
			Client current = clients.peekElement();
			try {
				Thread.sleep(current.getProcessingTime() * 100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			clients.popElement();
			lastBusyTime = LocalTime.now();
		}
		totalEmptyTime+=(int)Duration.between(this.lastBusyTime,LocalTime.now()).getSeconds();
		//System.out.println(totalEmptyTime);
		//System.out.println("Cashier " + this.id + " is closed");
		Application_2.writeTextToFile("Cashier " + this.id + " is closed"+"\n");
	}
}
