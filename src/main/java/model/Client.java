package model;

import java.time.LocalTime;
import java.util.concurrent.atomic.AtomicInteger;

public class Client {
	private static AtomicInteger id_inc = new AtomicInteger(0);
	private int id;
	private int arrivalTime;
	private int processingTime;
	private LocalTime timeAtArrival;
	
	static public void resetClientId() {
		Client.id_inc.set(0);
	}
	
	public Client(int arrivalTime,int processingTime) {
		this.id = id_inc.incrementAndGet();
		this.arrivalTime = arrivalTime;
		this.processingTime = processingTime;
		this.timeAtArrival = LocalTime.now();
	}
	
	public String toString() {
		return "ID:" + id + ";" + "AT:"+arrivalTime + ";" + "PT:" + processingTime;
	}

	public LocalTime getTimeAtArrival() {
		return this.timeAtArrival;
	}
	public int getId() {
		return id;
	}

	public int getProcessingTime() {
		return processingTime;
	}

//	public void setProcessingTime(int processingTime) {
//		this.processingTime = processingTime;
//	}
	
	
}
