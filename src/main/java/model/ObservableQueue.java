package model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;

public class ObservableQueue<E> {
	
	List<QueueObserver<E>> observers;
	private LinkedBlockingDeque<E> queue;
	private static AtomicInteger id_inc = new AtomicInteger(0);
	private int id;

	static public void resetId() {
		id_inc.set(0);
	}
	
	public ObservableQueue() {
		this.id = id_inc.getAndIncrement();
		observers = new ArrayList<QueueObserver<E>>();
		queue = new LinkedBlockingDeque<E>();
	}
	public int getId(){
		return this.id;
	}
	public void addObserver(QueueObserver<E> observer) {
		this.observers.add(observer);
	}
	
	public void pushElement(E e) {
		queue.addLast(e);
		for(QueueObserver<E> observer : observers) {
			try {
				observer.pushedElement(this, e);
			}catch(Exception exp) {
				System.err.println("Failed to notify queue push");
				exp.printStackTrace();
			}
		}
	}
	
	public E popElement() {
		E e = queue.poll();
		if(e != null) {
			for(QueueObserver<E> observer : observers) {
				try {
					observer.poppedElement(this, e);
				}catch(Exception exp) {
					System.err.println("Failed to notify queue push");
					exp.printStackTrace();
				}
			}
		}
		return e;
	}
	
	public E peekElement() {
		E e = queue.peekFirst();
		return e;
	}
	
	public int getSize() {
		return this.queue.size();
	}
	
	public boolean isEmpty() {
		return this.queue.isEmpty();
	}
}