package model;

public interface QueueObserver<E> {

	//void update(E e);
	
	void pushedElement(ObservableQueue<E> queue,E e);
	
	void poppedElement(ObservableQueue<E> queue,E e);
	
}
