package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.Color;
import javax.swing.Box;
import javax.swing.border.BevelBorder;

public class MainView extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField minTimeTextField;
	private JTextField maxTimeTextField;
	private JTextField minServiceTimeTextField;
	private JTextField maxServiceTimeTextField;
	private JTextField simulationTimeTextField;
	private JSpinner numberOfQueuesSpinner;
	private JButton btnBegin;
	private List<Box> queueBoxes;
	
	public String getMinTimeText(){
		return this.minTimeTextField.getText();
	}
	public String getMaxTimeText(){
		return this.maxTimeTextField.getText();
	}
	public String getMinServiceText(){
		return this.minServiceTimeTextField.getText();
	}
	public String getMaxServiceText(){
		return this.maxServiceTimeTextField.getText();
	}
	public String getSimulationTimeText(){
		return this.simulationTimeTextField.getText();
	}
	
	public int getNumberOfQueues() {
		return (Integer) this.numberOfQueuesSpinner.getValue();
	}
	
	
	public void btnBeginActionListener(final ActionListener actionListener) {
		btnBegin.addActionListener(actionListener);
	}
	public void setBtnBeginClickable(boolean stance) {
		btnBegin.setEnabled(stance);
	}
	
	
	public void popLabelOnBox(int queueNumber) {
		queueBoxes.get(queueNumber).remove(queueBoxes.get(queueNumber).getComponent(1));
		queueBoxes.get(queueNumber).revalidate();
		queueBoxes.get(queueNumber).repaint();
	}
	
	public void pushLabelOnBox(int queueNumber,String labelString) {
		JLabel label = new JLabel(labelString);
		queueBoxes.get(queueNumber).add(label);
		queueBoxes.get(queueNumber).revalidate();
	}

	public MainView(String name) {
		super(name);
		initialize();
	}

	public void initializeQueueBoxes(int numberOfQueues) {
		
		getContentPane().setVisible(true);
		for(Box each : queueBoxes) {
			getContentPane().remove(each);
		}
		getContentPane().revalidate();
		getContentPane().repaint();
		int xOffset = 20;
		int width = 98;
		queueBoxes = new ArrayList<Box>(numberOfQueues);
		for(int i=0;i<numberOfQueues;i++) {
			Box verticalBoxk = Box.createVerticalBox();
			verticalBoxk.setBorder(new BevelBorder(BevelBorder.LOWERED,null,null,null,null));
			verticalBoxk.setBackground(new Color(100, 149, 237));
			verticalBoxk.setForeground(new Color(124, 252, 0));
			verticalBoxk.setBounds(xOffset * (i+1) + width * i, 78, width, 355);
			getContentPane().add(verticalBoxk);
			queueBoxes.add(verticalBoxk);
			JButton button = new JButton("Cashier "+i);
			button.setEnabled(false);
			button.setBackground(Color.CYAN);
			verticalBoxk.add(button);
		}
		getContentPane().revalidate();
	}
	
	private void initialize() {
		
		this.setBounds(100, 100, 702, 483);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 78, 686, -70);
		this.getContentPane().add(panel);
		
		minTimeTextField = new JTextField();
		minTimeTextField.setText("2");
		minTimeTextField.setBounds(10, 47, 51, 20);
		this.getContentPane().add(minTimeTextField);
		minTimeTextField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Time between customers");
		lblNewLabel.setBounds(10, 16, 119, 20);
		this.getContentPane().add(lblNewLabel);
		
		maxTimeTextField = new JTextField();
		maxTimeTextField.setText("3");
		maxTimeTextField.setColumns(10);
		maxTimeTextField.setBounds(71, 47, 51, 20);
		this.getContentPane().add(maxTimeTextField);
		
		JLabel lblMin = new JLabel("Min");
		lblMin.setBounds(10, 35, 46, 14);
		this.getContentPane().add(lblMin);
		
		JLabel lblMax = new JLabel("Max");
		lblMax.setBounds(71, 35, 46, 14);
		this.getContentPane().add(lblMax);
		
		JLabel lblServiceTime = new JLabel("Service time");
		lblServiceTime.setBounds(169, 16, 119, 20);
		this.getContentPane().add(lblServiceTime);
		
		minServiceTimeTextField = new JTextField();
		minServiceTimeTextField.setText("2");
		minServiceTimeTextField.setColumns(10);
		minServiceTimeTextField.setBounds(169, 49, 51, 20);
		this.getContentPane().add(minServiceTimeTextField);
		
		maxServiceTimeTextField = new JTextField();
		maxServiceTimeTextField.setText("5");
		maxServiceTimeTextField.setColumns(10);
		maxServiceTimeTextField.setBounds(230, 49, 51, 20);
		this.getContentPane().add(maxServiceTimeTextField);
		
		JLabel label = new JLabel("Max");
		label.setBounds(230, 34, 46, 14);
		this.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("Min");
		label_1.setBounds(169, 34, 46, 14);
		this.getContentPane().add(label_1);
		
		numberOfQueuesSpinner = new JSpinner();
		numberOfQueuesSpinner.setModel(new SpinnerNumberModel(2, 1, 4, 1));
		numberOfQueuesSpinner.setBounds(457, 19, 35, 20);
		this.getContentPane().add(numberOfQueuesSpinner);
		
		JLabel lblNumberOfQueues = new JLabel("Number of queues");
		lblNumberOfQueues.setBounds(325, 19, 119, 20);
		this.getContentPane().add(lblNumberOfQueues);
		
		JLabel lblSimulationTime = new JLabel("Simulation time (sec)");
		lblSimulationTime.setBounds(325, 47, 133, 20);
		this.getContentPane().add(lblSimulationTime);
		
		simulationTimeTextField = new JTextField();
		simulationTimeTextField.setText("10");
		simulationTimeTextField.setBounds(457, 47, 35, 20);
		this.getContentPane().add(simulationTimeTextField);
		simulationTimeTextField.setColumns(10);
		
		btnBegin = new JButton("SIMULATE");
		btnBegin.setForeground(Color.BLACK);
		btnBegin.setBackground(Color.GREEN);
		btnBegin.setFont(new Font("Lucida Sans", Font.BOLD, 15));
		btnBegin.setBounds(528, 19, 133, 47);
		this.getContentPane().add(btnBegin);
		
		queueBoxes = new ArrayList<Box>(0);	
	}
}
